package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static com.epam.rd.java.basic.task8.controller.FlowerXMLConstants.*;

import java.util.Objects;

public class DOMControllerUtil {

    static Flower parseFlower(Node node){
        Flower flower = new Flower();

        if (node.getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) node;
            flower.setName(getTagValue(NAME_TAG, element));
            flower.setSoil(parseSoil(element));
            flower.setVisualParameters(parseVisualParameters(element));
            flower.setOrigin(getTagValue(ORIGIN_TAG, element));
            flower.setGrowingTips(parseGrowingTips(element));
            flower.setMultiplying(parseMultiplying(element));
        }
        return flower;
    }


    static Multiplying parseMultiplying(Element element) {
        Multiplying multiplying = Multiplying.getByValue(getTagValue(MULTIPLYING_TAG, element));
        return multiplying;
    }

    static GrowingTips parseGrowingTips(Element element) {
        GrowingTips growingTips = new GrowingTips();
        growingTips.setTemperature(parseTemperature(element));
        growingTips.setLighting(parseLighting(LIGHTING_TAG, element));
        growingTips.setWatering(parseWatering(WATERING_TAG, element));
        return growingTips;
    }

    static Watering parseWatering(String tag, Element element) {
        Watering watering = new Watering();
        watering.setMeasure(getTagAttribute(tag, MEASURE_ATTRIBUTE, element));
        watering.setWatering(Integer.parseInt(getTagValue(tag, element)));
        return watering;
    }

    static Lighting parseLighting(String tag, Element element) {
        Lighting lighting = new Lighting();
        lighting.setLightRequiring(getTagAttribute(tag, LIGHT_REQUIRING_ATTRIBUTE, element));
        return lighting;
    }

    static Temperature parseTemperature(Element element) {
        Temperature temperature = new Temperature();
        temperature.setTemperature(Integer.parseInt(getTagValue(TEMPERATURE_TAG, element)));
        temperature.setMeasure(Objects.requireNonNull(getTagAttribute(TEMPERATURE_TAG, MEASURE_ATTRIBUTE, element)));
        return temperature;
    }

    static String getTagValue(String tag, Element element){
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }

    static String getTagAttribute(String tag, String attr, Element element){
        Node node = element.getElementsByTagName(tag).item(0);
        if (node.getNodeType() == Node.ELEMENT_NODE){
            Element nodeElement = (Element) node;
            return nodeElement.getAttribute(attr);
        }
        return null;
    }

    static Soil parseSoil(Element element){
        return Soil.getByValue(getTagValue(SOIL_TAG, element));
    }

    static FlowerLength parseFlowerLength(Element element){
        FlowerLength flowerLength = new FlowerLength();
        flowerLength.setMeasure(getTagAttribute(AVE_LEN_FLOWER_TAG, MEASURE_ATTRIBUTE, element));
        flowerLength.setLength(Integer.parseInt(getTagValue(AVE_LEN_FLOWER_TAG, element)));
        return flowerLength;
    }

    static VisualParameters parseVisualParameters(Element element){
        VisualParameters visualParameters = new VisualParameters();
        visualParameters.setLeafColour(getTagValue(LEAF_COLOUR_TAG, element));
        visualParameters.setStemColour(getTagValue(STEM_COLOUR_TAG, element));
        visualParameters.setAveLenFlower(parseFlowerLength(element));
        return visualParameters;
    }

    static Element createFlowersNode(Document document, Flowers flowers){
        Element rootElement = document.createElementNS(FLOWERS_XML_NAMESPACE, FLOWERS_TAG_NAME);
        rootElement.setAttribute(FLOWERS_XSI_ATTRIBUTE, FLOWERS_XSI);
        rootElement.setAttribute(FLOWERS_XSI_SCHEMA_LOCATION_ATTRIBUTE, FLOWERS_XSI_SCHEMA_LOCATION);

        for (Flower flower : flowers.getFlowers()) {
            rootElement.appendChild(createFlowerNode(document, flower));
        }
        return rootElement;
    }

    static Node createFlowerNode(Document document, Flower flower){
        Element flowerNode = document.createElement(FLOWER_TAG_NAME);


        flowerNode.appendChild(createTextNodeElement(document, NAME_TAG, flower.getName()));
        flowerNode.appendChild(createTextNodeElement(document, SOIL_TAG, flower.getSoil().getValue()));
        flowerNode.appendChild(createTextNodeElement(document, ORIGIN_TAG, flower.getOrigin()));
        flowerNode.appendChild(createVisualParameterElement(document, flower.getVisualParameters()));
        flowerNode.appendChild(createGrowingTips(document, flower.getGrowingTips()));
        flowerNode.appendChild(createTextNodeElement(document, MULTIPLYING_TAG, String.valueOf(flower.getMultiplying().getValue())));

        return flowerNode;
    }

    private static Node createTextNodeElement(Document document, String tagName, String value){
        Node node = document.createElement(tagName);
        node.appendChild(document.createTextNode(value));
        return node;
    }

    private static Node createVisualParameterElement(Document document, VisualParameters visualParameters){
        Node node = document.createElement(VISUAL_PARAMETERS_TAG);
        node.appendChild(createTextNodeElement(document, STEM_COLOUR_TAG, visualParameters.getStemColour()));
        node.appendChild(createTextNodeElement(document, LEAF_COLOUR_TAG, visualParameters.getLeafColour()));
        node.appendChild(createAveLenFlowerElement(document,  visualParameters.getAveLenFlower()));

        return node;
    }

    private static Node createAveLenFlowerElement(Document document, FlowerLength aveLenFlower){
        Element element = document.createElement(AVE_LEN_FLOWER_TAG);
        element.setAttribute(MEASURE_ATTRIBUTE, aveLenFlower.getMeasure());
        element.appendChild(document.createTextNode(String.valueOf(aveLenFlower.getLength())));
        return element;
    }

    private static Node createGrowingTips(Document document, GrowingTips growingTips){
        Node node = document.createElement(GROWING_TIPS_TAG);
        node.appendChild(createTemperatureElement(document, growingTips.getTemperature()));
        node.appendChild(createLightingElement(document, growingTips.getLighting()));
        node.appendChild(createWateringElement(document, growingTips.getWatering()));
        return node;
    }

    private static Node createWateringElement(Document document, Watering watering) {
        Element element = document.createElement(WATERING_TAG);
        element.setAttribute(MEASURE_ATTRIBUTE, watering.getMeasure());
        element.appendChild(document.createTextNode(String.valueOf(watering.getWatering())));
        return element;
    }

    private static Node createLightingElement(Document document, Lighting lighting) {
        Element element = document.createElement(LIGHTING_TAG);
        element.setAttribute(LIGHT_REQUIRING_ATTRIBUTE, lighting.getLightRequiring());
        return element;
    }

    private static Node createTemperatureElement(Document document, Temperature temperature) {
        Element element = document.createElement(TEMPERATURE_TAG);
        element.setAttribute(MEASURE_ATTRIBUTE, temperature.getMeasure());
        element.appendChild(document.createTextNode(String.valueOf(temperature.getTemperature())));
        return element;
    }

}
