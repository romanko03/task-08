package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Watering {
    private String measure;
    private Integer watering;
}
