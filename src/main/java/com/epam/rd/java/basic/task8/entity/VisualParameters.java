package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class VisualParameters implements Comparable<VisualParameters> {
    private String stemColour;
    private String leafColour;

    private FlowerLength aveLenFlower;

    @Override
    public int compareTo(VisualParameters o) {
        return Comparator
                .comparing((VisualParameters vp)->vp.getAveLenFlower().getLength(), Integer::compareTo)
                .thenComparing(VisualParameters::getStemColour, String::compareTo)
                .thenComparing(VisualParameters::getLeafColour, String::compareTo)
                .compare(this, o);

    }
}
