package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class Temperature {
    private String measure;
    private Integer temperature;

    public static class TemperatureComparator implements Comparator<Temperature> {
        @Override
        public int compare(Temperature o1, Temperature o2) {
            return Comparator
                    .comparing(Temperature::getTemperature, Integer::compare)
                    .compare(o1, o2);
        }
    }
}
