package com.epam.rd.java.basic.task8.entity;

public enum Soil {

    PODZOLIC("подзолистая"),
    UNPAVED("грунтовая"),
    SOD_PODZOLIC("дерново-подзолистая");

    private final String value;

    Soil(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }

    public static Soil getByValue(String value){
        for (Soil soil : values()) {
            if (soil.value.equals(value)){
                return soil;
            }
        }
        return null;
    }
}
