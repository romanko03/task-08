package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;

@Getter
public enum Multiplying {
    LEAVES("листья"), CUTTINGS("черенки"), SEEDS("семена");

    private final String value;

    Multiplying(String value) {
        this.value = value;
    }

    public static Multiplying getByValue(String value){
        for (Multiplying multiplying : values()) {
            if (multiplying.value.equals(value)){
                return multiplying;
            }
        }
        return null;
    }
}
