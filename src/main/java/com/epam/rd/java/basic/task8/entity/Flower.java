package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class Flower {

    private String name;
    private Soil soil;
    private String origin;

    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    private Multiplying multiplying;

    public static class NameComparator implements Comparator<Flower> {
        @Override
        public int compare(Flower o1, Flower o2) {
            return Comparator.comparing(Flower::getName, String::compareTo).compare(o1, o2);
        }
    }

    public static class VisualParametersComparator implements Comparator<Flower>{
        @Override
        public int compare(Flower o1, Flower o2) {
            return Comparator
                    .comparing(Flower::getVisualParameters, VisualParameters::compareTo)
                    .compare(o1, o2);
        }
    }

    public static class GrowingTipsComparator implements Comparator<Flower> {
        @Override
        public int compare(Flower o1, Flower o2) {
            return Comparator
                    .comparing(Flower::getGrowingTips, GrowingTips::compareTo)
                    .compare(o1, o2);
        }
    }
}
