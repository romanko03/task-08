package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class FlowerLength {
    private String measure;

    private Integer length;

    public static class LengthComparator implements Comparator<FlowerLength> {
        @Override
        public int compare(FlowerLength o1, FlowerLength o2) {
            return Comparator
                    .comparing(FlowerLength::getLength, Integer::compare)
                    .compare(o1, o2);
        }
    }

}
