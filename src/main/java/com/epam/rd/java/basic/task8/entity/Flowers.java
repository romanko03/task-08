package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Flowers {
    private List<Flower> flowers = new ArrayList<>();

    public boolean addFlower(Flower flower){
        return flowers.add(flower);
    }
}
