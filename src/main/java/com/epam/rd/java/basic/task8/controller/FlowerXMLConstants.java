package com.epam.rd.java.basic.task8.controller;

public interface FlowerXMLConstants {

    String FLOWERS_XML_NAMESPACE = "http://www.nure.ua";

    String FLOWERS_XSI_ATTRIBUTE = "xmlns:xsi";
    String FLOWERS_XSI = "http://www.w3.org/2001/XMLSchema-instance";

    String FLOWERS_XSI_SCHEMA_LOCATION_ATTRIBUTE = "xsi:schemaLocation";
    String FLOWERS_XSI_SCHEMA_LOCATION = "http://www.nure.ua input.xsd ";

    String FLOWERS_TAG_NAME = "flowers";
    String FLOWER_TAG_NAME = "flower";
    String VISUAL_PARAMETERS_TAG = "visualParameters";
    String GROWING_TIPS_TAG = "growingTips";
    String NAME_TAG = "name";
    String ORIGIN_TAG = "origin";
    String MEASURE_ATTRIBUTE = "measure";
    String LIGHT_REQUIRING_ATTRIBUTE = "lightRequiring";
    String MULTIPLYING_TAG = "multiplying";
    String TEMPERATURE_TAG = "temperature";
    String LIGHTING_TAG = "lighting";
    String WATERING_TAG = "watering";
    String AVE_LEN_FLOWER_TAG = "aveLenFlower";
    String LEAF_COLOUR_TAG = "leafColour";
    String STEM_COLOUR_TAG = "stemColour";
    String SOIL_TAG = "soil";
}
