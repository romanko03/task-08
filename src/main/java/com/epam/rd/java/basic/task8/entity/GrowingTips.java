package com.epam.rd.java.basic.task8.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class GrowingTips implements Comparable<GrowingTips> {
    private Temperature temperature;
    private Lighting lighting;
    private Watering watering;

    @Override
    public int compareTo(GrowingTips o) {
        return Comparator
                .comparing(GrowingTips::getTemperature, new Temperature.TemperatureComparator())
                .compare(this, o);
    }
}
