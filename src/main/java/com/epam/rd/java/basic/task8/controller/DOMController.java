package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import lombok.SneakyThrows;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;

import static com.epam.rd.java.basic.task8.controller.FlowerXMLConstants.FLOWER_TAG_NAME;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Document document;

	@SneakyThrows
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();

		document = db.parse(xmlFileName);
	}

	// PLACE YOUR CODE HERE
	public Flowers parse(){
		Flowers flowers = new Flowers();
		Element root = document.getDocumentElement();
		NodeList chields = root.getElementsByTagName(FLOWER_TAG_NAME);

		for (int i = 0; i < chields.getLength(); i++) {
			Node item = chields.item(i);
			Flower flower = DOMControllerUtil.parseFlower(item);
			flowers.addFlower(flower);
		}

		return flowers;
	}

	public static void write(String outputFileName, Flowers flowers) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document document = db.newDocument();
		Element root = DOMControllerUtil.createFlowersNode(document, flowers);
		document.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT,"yes");

		DOMSource source = new DOMSource(document);
		StreamResult file = new StreamResult(new File(outputFileName));

		t.transform(source, file);
	}

	public static void sortFlowers(Flowers flowers){
		flowers.getFlowers().sort(getDOMFlowersComparator());
	}

	private static Flower.NameComparator getDOMFlowersComparator() {
		return new Flower.NameComparator();
	}

}
